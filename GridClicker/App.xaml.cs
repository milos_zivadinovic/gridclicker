﻿using AutoMapper;
using GridClicker.Enums;
using GridClicker.Models;
using GridClicker.Models.DTOs;
using GridClicker.Services;
using GridClicker.Services.Interfaces;
using GridClicker.Views;
using Prism.Ioc;
using Prism.Unity;
using Serilog;
using System.Collections.Specialized;
using System.Configuration;
using System.Windows;

namespace GridClicker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell() => Container.Resolve<MainWindow>();

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            bool useMemoryStore = false;

            if (appSettings.Count != 0)
            {
                string memoryStoreString = appSettings["UseMemoryStore"];

                if (!string.IsNullOrEmpty(memoryStoreString))
                {
                    bool.TryParse(memoryStoreString.ToLowerInvariant(), out useMemoryStore);
                }
            }

            if (useMemoryStore)
            {
                containerRegistry.Register<IPostStore, InMemoryPostStore>();
            }
            else
            {
                containerRegistry.Register<IPostStore, RESTPostStore>();
            }

            containerRegistry
                .Register<IStateMachine<EFlipState>, FlipStateMachine>()
                .RegisterInstance(ObjectMapper())
                .RegisterInstance(Logger());

        }

        private ILogger Logger() => new LoggerConfiguration().WriteTo.File("log-.txt", rollingInterval: RollingInterval.Day).CreateLogger();

        private IMapper ObjectMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Post, PostDTO>();
            });

            return config.CreateMapper();
        }
    }
}
