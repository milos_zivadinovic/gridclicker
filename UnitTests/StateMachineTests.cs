using GridClicker.Enums;
using GridClicker.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    /// <summary>
    /// The only Unit Tests I've written for this project are for the state machine. This is because the states should be thoroughly tested, since 
    /// they're one of the most important parts of the application. Other parts, such as HTTP calls, random number generators or the MVVM structure
    /// provided by Prism are covered by their respective unit tests from their developers.
    /// </summary>
    [TestClass]
    public class StateMachineTests
    {
        [TestMethod]
        public void InitialStateIsID()
        {
            FlipStateMachine machine = new FlipStateMachine(null);

            Assert.AreEqual(EFlipState.ID, machine.Current());
        }

        [TestMethod]
        public void StateAfterIDIsUserID()
        {
            FlipStateMachine machine = new FlipStateMachine(null);

            machine.Next();

            Assert.AreEqual(EFlipState.USER_ID, machine.Current());
        }

        [TestMethod]
        public void StateAfterUserIDIsID()
        {
            FlipStateMachine machine = new FlipStateMachine(null);

            machine.Next();
            machine.Next();

            Assert.AreEqual(EFlipState.ID, machine.Current());
        }
    }
}
