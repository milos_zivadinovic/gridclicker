﻿namespace GridClicker.Services.Interfaces
{
    public interface IStateMachine<T>
    {
        T Current();
        T Next();
    }
}
