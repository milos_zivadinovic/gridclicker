﻿namespace GridClicker.Models.DTOs
{
    public class PostDTO
    {
        // Since we just have to use the Id and UserId (we're flip-flopping them) we can safely disregard everything
        public string UserId { get; set; }
        public string Id { get; set; }
    }
}
