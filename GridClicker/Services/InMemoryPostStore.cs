﻿using GridClicker.Models;
using GridClicker.Services.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GridClicker.Services
{
    /// <summary>
    /// If we want to test locally without any API calls, this is a nice alternative. Randomly generates the minimum viable data we need.
    /// </summary>
    public class InMemoryPostStore : IPostStore
    {
        private readonly Random randomnessProvider = new Random();
        readonly ILogger _logger = null;

        public InMemoryPostStore(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<IEnumerable<Post>> Get()
        {
            List<Post> result = new List<Post>();

            try
            {
                int numberOfPosts = 100;

                for (int i = 0; i < numberOfPosts; i++)
                {
                    result.Add(new Post()
                    {
                        Id = i,
                        UserId = randomnessProvider.Next(i, numberOfPosts),
                        Body = ((char)randomnessProvider.Next(65, numberOfPosts)).ToString(),
                        Title = ((char)randomnessProvider.Next(65, numberOfPosts)).ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(Get));
            }

            return await Task.FromResult(result);
        }
    }
}
