﻿using GridClicker.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GridClicker.Services.Interfaces
{
    public interface IPostStore
    {
        Task<IEnumerable<Post>> Get();
    }
}
