﻿using GridClicker.Models;
using GridClicker.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GridClicker.Services
{
    /// <summary>
    /// Used for testing purposes to simulate null returned from the API
    /// </summary>
    public class NullPostStore : IPostStore
    {
        public async Task<IEnumerable<Post>> Get() => null;
    }
}
