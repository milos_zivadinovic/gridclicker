﻿using AutoMapper;
using GridClicker.Enums;
using GridClicker.Models;
using GridClicker.Models.DTOs;
using GridClicker.Services.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;

namespace GridClicker.ViewModels
{
    /// <summary>
    /// ViewModel of MainWindow
    /// </summary>
    sealed class MainWindowViewModel : BindableBase
    {
        private readonly IPostStore _postStore = null;
        private readonly IMapper _mapper = null;
        private readonly IStateMachine<EFlipState> _stateMachine = null;
        private readonly ILogger _logger = null;

        public MainWindowViewModel(IPostStore postStore, IMapper mapper, IStateMachine<EFlipState> stateMachine, ILogger logger)
        {
            _postStore = postStore;
            _mapper = mapper;
            _stateMachine = stateMachine;
            _logger = logger;

            _logger.Debug("MainWindowViewModel Ctor");

            LoadData();
        }

        public List<PostDTO> Posts { get; private set; } = new List<PostDTO>();

        public ObservableCollection<string> PostsSelectedField { get; private set; } = new ObservableCollection<string>();

        private string _statusText;

        public string StatusText
        {
            get
            {
                return _statusText;
            }
            set
            {
                _statusText = value;
                RaisePropertyChanged(nameof(StatusText));
            }
        }

        // Called a semaphore since it uses three colours
        private SolidColorBrush _statusSemaphore;

        public SolidColorBrush StatusSemaphore
        {
            get
            {
                return _statusSemaphore;
            }
            set
            {
                _statusSemaphore = value;
                RaisePropertyChanged(nameof(StatusSemaphore));
            }
        }

        private DelegateCommand _clickedCommand = null;
        public DelegateCommand ClickedCommand => _clickedCommand ??= new DelegateCommand(ClickedCommandExecute);

        private DelegateCommand _reloadCommand = null;
        public DelegateCommand ReloadCommand => _reloadCommand ??= new DelegateCommand(LoadData);

        private void ClickedCommandExecute()
        {
            try
            {
                _stateMachine.Next();

                FlipData();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(ClickedCommandExecute));
            }
        }

        /// <summary>
        /// Gets data from provided service, tracks its status, converts it into a DTO and calls to determine which string to show
        /// </summary>
        private async void LoadData()
        {
            try
            {
                Posts.Clear();

                NewStatus(EStatus.LOADING);

                IEnumerable<Post> resultFromCall = await _postStore.Get();

                if (resultFromCall != null && resultFromCall.Any())
                {
                    NewStatus(EStatus.DONE);
                }
                else
                {
                    NewStatus(EStatus.FAILED);
                }

                Posts.AddRange(
                    _mapper.Map<IEnumerable<PostDTO>>(
                        resultFromCall));

                FlipData();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(LoadData));
            }

        }

        /// <summary>
        /// Determines should it show Id or UserId, depending on the state machine
        /// </summary>
        private void FlipData()
        {
            try
            {
                Func<PostDTO, string> delegateSelector = (_stateMachine.Current()) switch
                {
                    EFlipState.USER_ID => (p => p.UserId),
                    _ => (p => p.Id)
                };

                PostsSelectedField.Clear();

                PostsSelectedField.AddRange(
                    Posts.Select(delegateSelector));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(FlipData));
            }
        }

        /// <summary>
        /// Updates the loading info in status bar
        /// </summary>
        /// <param name="status">New status of update</param>
        private void NewStatus(EStatus status)
        {
            try
            {
                // One idea I had was to use switching to extract the keys and then apply them in the end to the properties
                // In the end, it seems to be the same code-wise; you got to switch through the states either way

                switch (status)
                {
                    case EStatus.LOADING:
                        (StatusText, StatusSemaphore) = (App.Current.Resources["LoadingInProgress"] as string, App.Current.Resources["SemaphoreWait"] as SolidColorBrush);
                        break;
                    case EStatus.DONE:
                        (StatusText, StatusSemaphore) = (App.Current.Resources["LoadingDone"] as string, App.Current.Resources["SemaphoreGo"] as SolidColorBrush);
                        break;
                    case EStatus.FAILED:
                        (StatusText, StatusSemaphore) = (App.Current.Resources["LoadingFailed"] as string, App.Current.Resources["SemaphoreStop"] as SolidColorBrush);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(NewStatus));
            }
        }

    }
}
