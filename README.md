# GridClicker

WPF application fetching data from jsonplaceholder API and rendering it on a 10x10 grid

Current release: v1.2.0

## Introduction

This application was developed in the span of three days, besides regular working hours. It was developed with Visual Studio 2019 Community edition and written in C# and XAML.

The application works in the following way:
*   Data gets loaded from jsonplaceholder
*   Parts of the data (in this case `id`) gets rendered on the 10x10 grid
*   When a button on the grid is pressed, all buttons start showing `userId` instead of `id`
*   Repeat as needed

I've also added a small status bar to the bottom and a buttom that forces data reload when needed.

An additional constraint that I've added to myself was to only use out-of-the-box controls, without creating custom ones (even though I've experimented with them in this project).

Below you'll find the overview of development, an overview of the technical implementation as well as problems encountered during development and a conclusion.

## Development overview

As seen by the Releases tab, as well as commit history and merge requests, this project was incremental, where each phase after v1.0.0 was conforming to the minimum requirements and was ready to ship to production.

At first I started with a basic appliction using the MVVM pattern and Prism as the MVVM framework. After that, I started separating the business logic and the UI layer, as well as creating a state machine that could handle button clicks (I could've used an existing solution, but I didn't want to pull a large dependency for a small two state state machine).

Most of the time up to this point I used a mock generator to generate data (since HTTP calls are expensive, as well as they take a bit more time than running from a local in-memory store). If wanted, you can enable this memory store via the `App.config` file and `UseMemoryStore` property.

Later I switched to the HTTP calling infrastructure. Thanks to IoC provided by Unity, this was  breeze. I've also added logging using Serilog - just in case and because I believe that there should be logs everywhere, even if they just log exceptions.

When I've managed to get HTTP calling to work with the application, I declared the v1.0.0 release and started moving on. Next step was edge case coverage / bugfixing as well as adding a status bar at the bottom.

The bottom status bar shows if the data has been loaded, as well offers a button that allows you to reload the data. The reloaded data honors the current state in the state machine.

I've also added unit tests for the state machine since it's the only *mission critical* part of the code that would benefit from unit testing.

After several commits and tweaks in the code, I've released versions v1.1.0 and v1.2.0 which represent the application required. Version v1.1.0 contains most of the stuff from version v1.2.0 except a better layout for buttons.

## Technical implementation

### Overview
* Labels, colours and some styles are deployed using ResourceDictionaries, so that they can be localized if needed or changed without changing critical code pieces
* Less-global styles, data templates and item containers are deployed locally per page
* Buttons are arranged with a mix of `ListView` and `UniformGrid` as the items panel
* MVVM is available through the application, as well as IoC and DI
* I've worked with abstractions instead of concrete types as much as I could
* Utilized a DTO in order to work with real data (probably an overkill, but it's a nice solution)
* During development I've used GitLab's project tracking tools - check them out if you want to see how the project was progressing

Other functionalities and patterns can be seen in the code.

### External libraries used
* AutoMapper - for mapping between `Post` and `PostDTO`
* Newtonsoft.Json - for parsing `Post` data
* Prism.Unity - MVVM provider as well as DI container
* Serilog - logging framework
* Serilog.Sinks.File - extension to Serilog that allows writing logs to a file

They were all deployed via NuGet.

## Problems encountered
Most of the problems I've encountered were in the UWP and WPF switch (since I mostly do UWP) where the APIs are a bit different. Other than that, there were some Prism issues, mostly since I've used to deploy my own MVVM frameworks and DI containers (which is a fun challenge, but most of the time unnecessary), but as well, nothing that documentation can't fix.

## Conclusion
The development of the `GridClicker` application was a fun challenge and I enjoyed working with it. On the other hand, I've also learned and encountered some fun things mostly related to XAML (e.g. the `UniformGrid` control; my first approach was to create a 10x10 `Grid`).