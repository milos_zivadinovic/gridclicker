﻿using GridClicker.Models;
using GridClicker.Services.Interfaces;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GridClicker.Services
{
    /// <summary>
    /// Gets Posts from the jsonplaceholder API
    /// </summary>
    public class RESTPostStore : IPostStore
    {
        private static readonly HttpClient client = new HttpClient();
        readonly ILogger _logger = null;

        public RESTPostStore(ILogger logger)
        {

            if (client != null)
            {
                client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }

            _logger = logger;
        }

        public async Task<IEnumerable<Post>> Get()
        {
            List<Post> result = new List<Post>();

            try
            {
                HttpResponseMessage response = await client.GetAsync("posts");

                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<List<Post>>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(Get));
            }

            return result;
        }
    }
}
