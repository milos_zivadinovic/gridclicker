﻿using GridClicker.Enums;
using GridClicker.Services.Interfaces;
using Serilog;
using System;

namespace GridClicker.Services
{
    /// <summary>
    /// In order to keep track of what is the current state of the grid, we need a state machine which will allow looping between its two elements.
    /// This was also created in order to minimize development errors, since the programmer doesn't need to know what the states are, just needs to call .Next()
    /// to advance (since we have two states that are looping, in more difficult scenarios we'd have some more decision logic inside
    /// </summary>
    public class FlipStateMachine : IStateMachine<EFlipState>
    {
        /// <summary>
        /// Just in case if somehow multiple threads get spawned and then they start messing with our state machine. Shouldn't happen
        /// since we have bound this to one button, but here's a nice use case: when the data gets loaded from the store and the button
        /// gets pressed this would cause issues
        /// </summary>
        private readonly object padlock = new object();

        private EFlipState _state = EFlipState.ID;
        readonly ILogger _logger = null;

        public FlipStateMachine(ILogger logger)
        {
            _logger = logger;
        }

        public EFlipState Current() => _state;

        public EFlipState Next()
        {
            try
            {
                lock (padlock)
                {
                    if (_state == EFlipState.ID)
                    {
                        _state = EFlipState.USER_ID;
                    }
                    else
                    {
                        _state = EFlipState.ID;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, nameof(Next));
            }

            return Current();
        }
    }
}
